-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2020 at 05:15 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `studentregistration`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `rollno` int(11) NOT NULL,
  `dob` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `class` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `section` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `annualmark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`, `role`, `rollno`, `dob`, `class`, `section`, `annualmark`) VALUES
(1, 'Admin', 'FYzrk4y83kw9W5l_N52JFRtljkAdMwmz', '$2y$13$nVG5jGuyy.MNsr5SoeB/3.PaN3gc24jsuQAGgFzSy7Kx5eC1ygk7S', NULL, 'mohanty.sirsananda@gmail.com', 10, 1585029332, 1585029332, 'wAkCR-5ZOMZ4qvVg4FSx4uvYtgSuykU8_1585029332', 'admin', 0, '', '', '', 0),
(2, 'Sirsananda', 'ewaqif0YvtaQu2MrS06cUm92SLkCj3zm', '$2y$13$FuW0vuUKHXJwRVSqMyAA3e7mf1Ijb8Uz88ixNAhnQ6MO2zWgzGtd2', NULL, 'kumartapan891@gmail.com', 10, 1585038503, 1585038503, 'Igi6SkeB1LVaJcL5JmGdFDXDQMamZIUv_1585038503', 'student', 2147483647, '18/03/1996', 'mca', 'b', 10),
(9, 'litu', '-PLJKGflbg0YSH9RQiLvDJ6d9yfynv3s', '$2y$13$8Pm86p6hGm33l.KL4n3uT.Yv9xlIjrPe2f90y4wFOT/GYZeansl1i', NULL, 'gaurav@gmail.com', 9, 1585122317, 1585122317, 'shhi1hSDOxsZDcYL4_qKA0idWz1tOyw0_1585122317', 'student', 2147483647, '18/03/1996', 'mca', 'b', 10),
(10, 'sdfg', 'KEt6k_Fi0kx7v7AgYfqbyzf35R3FaicS', '$2y$13$wuUkzlhCrhuGg0oYnpjuw.dQeZ3bB7CKR1OM4fn2.VKnwsHGGQOD6', NULL, 'dfg@gmail.com', 9, 1585122367, 1585122367, 'YiebJchQBf0LH_k2w9Q5vKc925aksEX3_1585122367', 'student', 2147483647, '18/03/1996', 'mba', 'a', 10),
(13, 'Sirsa', 'gqDzDQQUmjTqbSFKpU7HBizeo-HOGSpf', '$2y$13$44Iry.N69YzNfM/g/AM1YeNJHhFpLNK8hJmLIsPFYY7f5.W1nCll2', NULL, 'rukhsanaparween502@gmail.com', 10, 1585135461, 1585135461, 'zGeSheGZwyeSvbzxU7zH3gAY0XElCluF_1585135461', 'student', 456789, '18/03/1996', 'mba', 'b', 10),
(14, 'dfghjk', 'ORxb22OzkhyOiF74LqoxTpPOxfvhblPK', '$2y$13$S9matAe8e1iZtWzuiWpVxOlFWUx/TCn7YL5DLsNBsXg1TBotN4e/S', NULL, 'kjhgfd@gmail.com', 10, 1585210027, 1585210027, 'mrG_qNXGCQUudgtJxNNr_48q7AHZWARr_1585210026', 'student', 1596874, '18/03/1996', 'mba', 'a', 10),
(15, 'sw', 'al2eRw1mdV0bqhVMGzLNVu4ZAM9jvlvx', '$2y$13$Qch9XdblM8GhmowOIrE3m.Z1E9AND91RSjY3GbvraVGHIdKtU31DC', NULL, 'se@gmail.com', 10, 1585210385, 1585210385, 'Ow_vlhqGRzXQ5AQYjPtvj7L-5WMziE3x_1585210385', 'student', 456789, '18/03/1996', 'm', 'b', 10),
(16, 'drt', 'wTYaUE3ZtbZBXRJwW8joIt-xGeUV4Sm-', '$2y$13$WBLiP64PHUsqyO68r0mThesolVZLqRWNgzR0pmhBygfnvBVM3MoFa', NULL, 'sdcv@gmail.com', 10, 1585210805, 1585210805, 'R_PuG3Kcp_48gMpQjU5w4sJFS-5yerBM_1585210805', 'student', 456789, '18/03/1996', 'm', 'b', 10),
(17, 'Sirsanandagh', 'mcf-aINjLHmjT_2vO4cuV9jErDhyDrku', '$2y$13$ubdbO2ahnR59PSRK9w0dE.FsAlQ9huoIa1DPbfJJC39/l7sRsJkOu', NULL, 'kumartap891@gmail.com', 10, 1585211443, 1585211443, 'XqTy09LyLNJ313WpnXh-0HLFEtdvHMKg_1585211443', 'student', 456789, '18/03/1996', 'm', 'k', 102),
(18, '', '', '', NULL, '', 10, 1585213906, 1585213906, NULL, '', 0, '', '', '', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
